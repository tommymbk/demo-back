<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table("produits")->insert([
        ["nom"=>"viande", "categorie"=>"alimentation", "prix"=>"2000"],
         ["nom"=>"montre", "categorie"=>"accessoire", "prix"=>"7000"],
         ["nom"=>"radio", "categorie"=>"electronique", "prix"=>"4200"]
        ]);
    }
}
