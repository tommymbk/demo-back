<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;

     protected $fillable = ["nom", "categorie", "prix"];

    public function produit(){
       return  $this->belongsTo(Produit::class);
}
}