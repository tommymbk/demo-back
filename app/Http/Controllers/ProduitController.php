<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

  /**
     * @OA\Get(
     *      path="/produit",
     *      operationId="getAllProduit",
     *      tags={"Tests"},

     *      summary="Get produit",
     *      description="",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function index()
    {
        $produit = Produit::all();

        return $produit;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**     @OA\POST(
*          path="/produit",
*           operationId="ajouterProduit",
*         tags={"Tests"},
*          summary="ajouter produit with data = []",
*           description="",
*         @OA\RequestBody(
*               @OA\MediaType(
*                   mediaType="application/JSON",
*                   @OA\Schema(
*                       @OA\Property(
*                           type="Object",  
*                           @OA\Property(
*                              property="nom",
*                               type="string"),
*                           @OA\Property(
*                               property="categorie",
*                               type="string"),
*                           @OA\Property(
*                               property="prix",
*                               type="interger")
*                             ),
*                        example={
*                               "nom"="example nom",
*                               "categorie"="example categorie",
*                               "prix"="example prix"}
 *)
*                )
*),
*          @OA\Response(
*               response=201,
*              description="Successful operation",
*           ),
*           @OA\Response(
*               response=401,
*               description="Unauthenticated",
*           ),
*           @OA\Response(
*               response=403,
*               description="Forbidden"
*           ),
*      @OA\Response(
*           response=400,
*           description="Bad Request"
*      ),
 *     @OA\Response(
*           response=404,
*           description="not found"
*        ),
*       
*)
*/    
    
    public function store(Request $request)
    {
      if( Produit::create($request->all())){
        return new Response($request, status: 201);
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Get(
     *      path="/produit/{produitID}",
     *      operationId="getProduitbyID",
     *      tags={"Tests"},
     *      summary="Get Produit with ID",
     *      description="",
     *@OA\Parameter(
     *      name="produitID",
     *      in="path",
     *      required=false,
     *      @OA\Schema(
     *           type="interger"
     *      )
     *   ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )*/
    public function show($id)
    {
        return Produit::find($id); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    
    
/**     @OA\PATCH(
*
*          path="/produit/{produitID}",
*           operationId="updateProduit",
*         tags={"Tests"},
*          summary="update produit with id = produitID data = []",
*           description="",
*     @OA\Parameter(
*           name="produitID",
*           in="path",
*           required=false,
*           @OA\Schema(
*                type="interger"
*           )
*        ),
*         @OA\RequestBody(
*               @OA\MediaType(
*                   mediaType="application/JSON",
*                   @OA\Schema(
*                       @OA\Property(
*                           type="Object",  
*                           @OA\Property(
*                              property="nom",
*                               type="string"),
*                           @OA\Property(
*                               property="categorie",
*                               type="string"),
*                           @OA\Property(
*                               property="prix",
*                               type="interger")
*                             ),
*                        example={
*                               "nom"="example nom",
*                               "categorie"="example categorie",
*                               "prix"="example prix"}
 *)
*                )
*),
*          @OA\Response(
*               response=201,
*              description="Successful operation",
*               @OA\MediaType(
*                mediaType="application/json",
*           )
*           ),
*           @OA\Response(
*               response=401,
*               description="Unauthenticated",
*           ),
*           @OA\Response(
*               response=403,
*               description="Forbidden"
*           ),
*      @OA\Response(
*           response=400,
*           description="Bad Request"
*      ),
 *     @OA\Response(
*           response=404,
*           description="not found"
*        ),
*       
*)
*/    
    public function update(Request $request, $id)
    {
        $produit = Produit::find($id);
        if( $produit->update($request->all())){
        return new Response($request, status: 201);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\DELETE(
     *      path="/produit/{produitID}",
     *      operationId="delProduitbyID",
     *      tags={"Tests"},
     *      summary="delete Produit with ID",
     *      description="",
     *@OA\Parameter(
     *      name="produitID",
     *      in="path",
     *      required=false,
     *      @OA\Schema(
     *           type="interger"
     *      )
     *   ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      )
* )
*/
    public function destroy($id)
    {
        $prod = Produit::find($id);
        if($prod){$prod->delete();}
        
    }
}
