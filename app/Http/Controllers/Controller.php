<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
/**
* @OA\OpenApi(
*       @OA\Info(
*           version="1.0",
*           title="Api CRUD Produit Documentation",
*             description="demo CRUD Produit",
*                   ),
*       @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Demo API Server"
 *              )
*           )
 */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
