<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProduitTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
     public function testPost()
    {
        $response = [
            'nom'=>'ordinateur',
            'categorie'=>'electronique',
            'prix'=>140000
        ];

        $this->json('POST', route('produit.ajouter'),$response)
            ->assertStatus(201);
    }

    public function testGet()
    {
        $reponse = $this->json('GET', route('produit'));
        $reponse->assertStatus(200);
    }

    public function testShow()
    {
        $reponse = $this->json('GET', route('produit.show', 1));
        $reponse->assertStatus(200);
    }

    public function testUpdate()
    {
        $response = [
            'nom'=>'calculatrice',
            'prix'=>1000
        ];

        $this->json('PATCH', route('produit.update', 1),$response)
            ->assertStatus(201);
    }

    public function testDel()
    {   
        $this->json('DELETE', route('produit.del', 1))
            ->assertStatus(200);
    }
}
