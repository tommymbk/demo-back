<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProduitController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/produit", [ProduitController::class, "index"])->name("produit");
Route::post("/produit", [ProduitController::class, "store"])->name("produit.ajouter");
Route::delete("/produit/{produit}", [ProduitController::class, "destroy"])->name("produit.del");
Route::patch("/produit/{produit}", [ProduitController::class, "update"])->name("produit.update");
Route::get("/produit/{produit}", [ProduitController::class, "show"])->name("produit.show");